﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Staff.
    /// </summary>
    [Serializable]
    public class Cat_StaffCollection : BusinessCollection<Cat_Staff>
    {
    }
}