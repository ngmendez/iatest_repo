﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo AspNetUsers.
    /// </summary>
    [DataContract, Serializable]
    public partial class AspNetUsers : BusinessObject<AspNetUsers, AspNetUsersCollection>
    {
        #region Constantes Columnas
protected const string ColumnaId = "Id";
protected const string ColumnaEmail = "Email";
protected const string ColumnaEmailConfirmed = "EmailConfirmed";
protected const string ColumnaPasswordHash = "PasswordHash";
protected const string ColumnaSecurityStamp = "SecurityStamp";
protected const string ColumnaPhoneNumber = "PhoneNumber";
protected const string ColumnaPhoneNumberConfirmed = "PhoneNumberConfirmed";
protected const string ColumnaTwoFactorEnabled = "TwoFactorEnabled";
protected const string ColumnaLockoutEndDateUtc = "LockoutEndDateUtc";
protected const string ColumnaLockoutEnabled = "LockoutEnabled";
protected const string ColumnaAccessFailedCount = "AccessFailedCount";
protected const string ColumnaUserName = "UserName";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase AspNetUsers.
        /// </summary>
        public AspNetUsers()
            : base()
        {
            this.TableName = "AspNetUsers";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaId, DbType.String, true)]
        public string Id { get; set; }

[DataMember, BdAction(ColumnaEmail, DbType.String, true, true)]
        public string Email { get; set; }

[DataMember, BdAction(ColumnaEmailConfirmed, DbType.Boolean, true, true)]
        public bool? EmailConfirmed { get; set; }

[DataMember, BdAction(ColumnaPasswordHash, DbType.String, true, true)]
        public string PasswordHash { get; set; }

[DataMember, BdAction(ColumnaSecurityStamp, DbType.String, true, true)]
        public string SecurityStamp { get; set; }

[DataMember, BdAction(ColumnaPhoneNumber, DbType.String, true, true)]
        public string PhoneNumber { get; set; }

[DataMember, BdAction(ColumnaPhoneNumberConfirmed, DbType.Boolean, true, true)]
        public bool? PhoneNumberConfirmed { get; set; }

[DataMember, BdAction(ColumnaTwoFactorEnabled, DbType.Boolean, true, true)]
        public bool? TwoFactorEnabled { get; set; }

[DataMember, BdAction(ColumnaLockoutEndDateUtc, DbType.DateTime, true, true)]
        public DateTime? LockoutEndDateUtc { get; set; }

[DataMember, BdAction(ColumnaLockoutEnabled, DbType.Boolean, true, true)]
        public bool? LockoutEnabled { get; set; }

[DataMember, BdAction(ColumnaAccessFailedCount, DbType.Int32, true, true)]
        public int? AccessFailedCount { get; set; }

[DataMember, BdAction(ColumnaUserName, DbType.String, true, true)]
        public string UserName { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.Id = this.ObtenerValorColumna<string>(ColumnaId);
this.Email = this.ObtenerValorColumna<string>(ColumnaEmail);
this.EmailConfirmed = this.ObtenerValorColumna<bool?>(ColumnaEmailConfirmed);
this.PasswordHash = this.ObtenerValorColumna<string>(ColumnaPasswordHash);
this.SecurityStamp = this.ObtenerValorColumna<string>(ColumnaSecurityStamp);
this.PhoneNumber = this.ObtenerValorColumna<string>(ColumnaPhoneNumber);
this.PhoneNumberConfirmed = this.ObtenerValorColumna<bool?>(ColumnaPhoneNumberConfirmed);
this.TwoFactorEnabled = this.ObtenerValorColumna<bool?>(ColumnaTwoFactorEnabled);
this.LockoutEndDateUtc = this.ObtenerValorColumna<DateTime?>(ColumnaLockoutEndDateUtc);
this.LockoutEnabled = this.ObtenerValorColumna<bool?>(ColumnaLockoutEnabled);
this.AccessFailedCount = this.ObtenerValorColumna<int?>(ColumnaAccessFailedCount);
this.UserName = this.ObtenerValorColumna<string>(ColumnaUserName);
        }
        #endregion
    }
}