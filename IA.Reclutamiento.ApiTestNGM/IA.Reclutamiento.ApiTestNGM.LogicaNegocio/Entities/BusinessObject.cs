﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Managers;

    /// <summary>
    /// Clase que representa un objeto de negocio base, del cual deben heredar todos los objetos de negocio de la aplicación.
    /// </summary>
    /// <typeparam name="BO">Clase que debe implementar la interfaz de objeto de negocio IObjectBase.</typeparam>
    /// <typeparam name="BC">Clase que debe implementar la interfaz ICollectionBase.</typeparam>
    [DataContract, Serializable]
    public abstract class BusinessObject<BO, BC> : ObjectManager<BO, BC>
        where BO : IObjectBase
        where BC : ICollectionBase<BO>
    {
        /// <summary>
        /// Inizializa un nuevo objeto de negocio.
        /// </summary>
        public BusinessObject()
        {
            this.ConnectionStringName = "DefaultConnection";
        }
    }
}