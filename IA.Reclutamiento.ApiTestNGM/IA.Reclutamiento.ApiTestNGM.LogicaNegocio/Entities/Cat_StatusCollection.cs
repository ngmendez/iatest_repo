﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Status.
    /// </summary>
    [Serializable]
    public class Cat_StatusCollection : BusinessCollection<Cat_Status>
    {
    }
}