﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Staff.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Staff : BusinessObject<Cat_Staff, Cat_StaffCollection>
    {
        #region Constantes Columnas
protected const string ColumnaStaffId = "StaffId";
protected const string ColumnaStaff_PositionId = "Staff_PositionId";
protected const string ColumnaStaff_StatusId = "Staff_StatusId";
protected const string ColumnaStaff_FirstName = "Staff_FirstName";
protected const string ColumnaStaff_LastName = "Staff_LastName";
protected const string ColumnaStaff_Email = "Staff_Email";
protected const string ColumnaStaff_Bithdate = "Staff_Bithdate";
protected const string ColumnaStaff_ControlNumber = "Staff_ControlNumber";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Staff.
        /// </summary>
        public Cat_Staff()
            : base()
        {
            this.TableName = "tbl_cat_Staff";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaStaffId, DbType.Int32, true)]
        public int? StaffId { get; set; }

[DataMember, BdAction(ColumnaStaff_PositionId, DbType.Int32, true, true)]
        public int? Staff_PositionId { get; set; }

[DataMember, BdAction(ColumnaStaff_StatusId, DbType.Int32, true, true)]
        public int? Staff_StatusId { get; set; }

[DataMember, BdAction(ColumnaStaff_FirstName, DbType.AnsiString, true, true)]
        public string Staff_FirstName { get; set; }

[DataMember, BdAction(ColumnaStaff_LastName, DbType.AnsiString, true, true)]
        public string Staff_LastName { get; set; }

[DataMember, BdAction(ColumnaStaff_Email, DbType.AnsiString, true, true)]
        public string Staff_Email { get; set; }

[DataMember, BdAction(ColumnaStaff_Bithdate, DbType.Date, true, true)]
        public DateTime? Staff_Bithdate { get; set; }

[DataMember, BdAction(ColumnaStaff_ControlNumber, DbType.AnsiString, true, true)]
        public string Staff_ControlNumber { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.StaffId = this.ObtenerValorColumna<int?>(ColumnaStaffId);
this.Staff_PositionId = this.ObtenerValorColumna<int?>(ColumnaStaff_PositionId);
this.Staff_StatusId = this.ObtenerValorColumna<int?>(ColumnaStaff_StatusId);
this.Staff_FirstName = this.ObtenerValorColumna<string>(ColumnaStaff_FirstName);
this.Staff_LastName = this.ObtenerValorColumna<string>(ColumnaStaff_LastName);
this.Staff_Email = this.ObtenerValorColumna<string>(ColumnaStaff_Email);
this.Staff_Bithdate = this.ObtenerValorColumna<DateTime?>(ColumnaStaff_Bithdate);
this.Staff_ControlNumber = this.ObtenerValorColumna<string>(ColumnaStaff_ControlNumber);
        }
        #endregion
    }
}