﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo Cat_Position.
    /// </summary>
    [Serializable]
    public class Cat_PositionCollection : BusinessCollection<Cat_Position>
    {
    }
}