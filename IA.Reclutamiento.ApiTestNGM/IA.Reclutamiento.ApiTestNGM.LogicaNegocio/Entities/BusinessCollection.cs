﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Managers;

    /// <summary>
    /// Representa una colección generica de objetos de negocio, de la cual deben heredar las colecciones de negocio especificas.
    /// </summary>
    /// <typeparam name="BO">Clase que debe implementar la interfaz de objeto de negocio IObjectBase.</typeparam>
    [DataContract, Serializable]
    public abstract class BusinessCollection<BO> : CollectionManager<BO>
        where BO : IObjectBase
    {
    }
}