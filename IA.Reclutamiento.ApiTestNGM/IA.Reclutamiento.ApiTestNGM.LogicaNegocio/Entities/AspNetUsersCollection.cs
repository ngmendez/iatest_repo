﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Representa una colección de entidades de tipo AspNetUsers.
    /// </summary>
    [Serializable]
    public class AspNetUsersCollection : BusinessCollection<AspNetUsers>
    {
    }
}