﻿namespace IA.LogicaNegocio.Entities
{
    using System;
    using System.Data;
    using System.Runtime.Serialization;
    using NGM.LogicaNegocio.Attributes;

    /// <summary>
    /// Representa una entidad de negocio de tipo Cat_Position.
    /// </summary>
    [DataContract, Serializable]
    public partial class Cat_Position : BusinessObject<Cat_Position, Cat_PositionCollection>
    {
        #region Constantes Columnas
protected const string ColumnaPositionId = "PositionId";
protected const string ColumnaPosition_Description = "Position_Description";
protected const string ColumnaPosition_SalaryMin = "Position_SalaryMin";
protected const string ColumnaPosition_SalaryMax = "Position_SalaryMax";
protected const string ColumnaPsotion_StatusId = "Psotion_StatusId";
        #endregion
        #region Constructores Default
        /// <summary>
        /// Inicializa una nueva instancia de la clase Cat_Position.
        /// </summary>
        public Cat_Position()
            : base()
        {
            this.TableName = "tbl_cat_Position";
            this.EsquemaBd = "dbo";
        }
        #endregion
        #region Propiedades Default
[DataMember, BdAction(ColumnaPositionId, DbType.Int32, true)]
        public int? PositionId { get; set; }

[DataMember, BdAction(ColumnaPosition_Description, DbType.AnsiString, true, true)]
        public string Position_Description { get; set; }

[DataMember, BdAction(ColumnaPosition_SalaryMin, DbType.Double, true, true)]
        public double? Position_SalaryMin { get; set; }

[DataMember, BdAction(ColumnaPosition_SalaryMax, DbType.Double, true, true)]
        public double? Position_SalaryMax { get; set; }

[DataMember, BdAction(ColumnaPsotion_StatusId, DbType.Int32, true, true)]
        public int? Psotion_StatusId { get; set; }
        #endregion
        #region Metodos Default
/// <summary>
        /// Llena las propiedades de la entidad.
        /// </summary>
        protected override void LlenarEntidad()
        {
this.PositionId = this.ObtenerValorColumna<int?>(ColumnaPositionId);
this.Position_Description = this.ObtenerValorColumna<string>(ColumnaPosition_Description);
this.Position_SalaryMin = this.ObtenerValorColumna<double?>(ColumnaPosition_SalaryMin);
this.Position_SalaryMax = this.ObtenerValorColumna<double?>(ColumnaPosition_SalaryMax);
this.Psotion_StatusId = this.ObtenerValorColumna<int?>(ColumnaPsotion_StatusId);
        }
        #endregion
    }
}