﻿using IA.LogicaNegocio.Entities;
using IA.Reclutamiento.ApiTestNGM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IA.Reclutamiento.ApiTestNGM.Controllers
{
    public class PositionController : Controller
    {
        // GET: Position
        public ActionResult Index(string dsearch)
        {
            PositionModel model = new Models.PositionModel();
            Cat_PositionCollection lstResult = new Cat_PositionCollection();

            // Obtener registro de la entidad estatus que corresponde a estatus ACTIVO
            Cat_Status entStatus = new Cat_Status()
            {
                Status_Status = "ACTIVO"
            };
            entStatus = entStatus.GetFirtByData();

            if (entStatus != null)
            {
                // Obtener registros que tengan estatus ACTIVO
                Cat_Position entObj = new Cat_Position()
                {
                    Psotion_StatusId = entStatus.StatusId
                };
                Cat_PositionCollection lstObj = entObj.GetCollectionByData();

                if (String.IsNullOrEmpty(dsearch))
                {
                    model.lstPosition = lstObj;
                }
                // Si se recibe valor de busqueda
                else
                {
                    foreach (Cat_Position objEnt in lstObj)
                    {
                        if (Utilities.General.Contains(objEnt.Position_Description, dsearch, StringComparison.CurrentCultureIgnoreCase))
                        {
                            lstResult.Add(objEnt);
                        }
                    }

                    model.lstPosition = lstResult;
                    model.dsearch = dsearch;
                }
            }

            return View(model);
        }
    }
}