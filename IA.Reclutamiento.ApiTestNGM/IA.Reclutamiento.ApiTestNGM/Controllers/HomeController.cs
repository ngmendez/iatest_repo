﻿using IA.LogicaNegocio.Entities;
using IA.Reclutamiento.ApiTestNGM.Models;
using IA.Reclutamiento.ApiTestNGM.Providers;
using IA.Reclutamiento.ApiTestNGM.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IA.Reclutamiento.ApiTestNGM.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string dsearch)
        {
            if (System.Web.HttpContext.Current.Session["userInfo"] == null)
            {
                Response.Redirect("Home/Login");
            }

            StaffModel model = new Models.StaffModel();
            Cat_StaffCollection lstResult = new Cat_StaffCollection();

            // Obtener registro de la entidad estatus que corresponde a estatus ACTIVO
            Cat_Status entStatus = new Cat_Status()
            {
                Status_Status = "ACTIVO"
            };
            entStatus = entStatus.GetFirtByData();

            if (entStatus != null)
            {
                // Obtener personal que tenga estatus ACTIVO
                Cat_Staff entStaff = new Cat_Staff()
                {
                    Staff_StatusId = entStatus.StatusId
                };
                Cat_StaffCollection lstStaff = entStaff.GetCollectionByData();

                if (String.IsNullOrEmpty(dsearch))
                {
                    model.lstStaff = lstStaff;
                }
                // Si se recibe valor de busqueda
                else
                {
                    foreach(Cat_Staff objStaff in lstStaff)
                    {
                        if (General.Contains(objStaff.Staff_FirstName, dsearch, StringComparison.CurrentCultureIgnoreCase) ||
                            General.Contains(objStaff.Staff_LastName, dsearch, StringComparison.CurrentCultureIgnoreCase))
                        {
                            lstResult.Add(objStaff);
                        }
                    }

                    model.lstStaff = lstResult;
                    model.dsearch = dsearch;
                }
            }

            return View(model);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SetSession(string userInfo)
        {
            MessageBaseModel model = new Models.MessageBaseModel();

            if (String.IsNullOrEmpty(userInfo))
            {
                model.Correct = false;
                model.Message = "No se pudo encontrar información de usuario.";
            }
            else
            {
                AspNetUsers entUser = AspNetUsers.GetOne(userInfo);

                if (entUser != null)
                {
                    System.Web.HttpContext.Current.Session["userInfo"] = entUser;
                    model.Correct = true;
                }
            }

            return this.Json(model);
        }
    }
}
