﻿using IA.LogicaNegocio.Entities;
using IA.Reclutamiento.ApiTestNGM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IA.Reclutamiento.ApiTestNGM.Controllers
{
    public class StatusController : Controller
    {
        // GET: Status
        public ActionResult Index(string dsearch)
        {
            StatusModel model = new Models.StatusModel();
            Cat_StatusCollection lstResult = new Cat_StatusCollection();

            if (String.IsNullOrEmpty(dsearch))
            {
                model.lstStatus = Cat_Status.GetAll();
            }
            // Si se recibe valor de busqueda
            else
            {
                foreach (Cat_Status objEnt in Cat_Status.GetAll())
                {
                    if (Utilities.General.Contains(objEnt.Status_Status, dsearch, StringComparison.CurrentCultureIgnoreCase))
                    {
                        lstResult.Add(objEnt);
                    }
                }

                model.lstStatus = lstResult;
                model.dsearch = dsearch;
            }

            return View(model);
        }
    }
}