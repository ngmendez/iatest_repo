﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IA.Reclutamiento.ApiTestNGM.Models;
using IA.LogicaNegocio.Entities;
using IA.Reclutamiento.ApiTestNGM.Utilities;

namespace IA.Reclutamiento.ApiTestNGM.Controllers
{
    public class StaffApiController : ApiController
    {
        [Route("api/StaffApi/Save")]
        [HttpGet, HttpPost]
        public MessageBaseModel SaveStaff(Cat_Staff staffInfo)
        {
            MessageBaseModel model = new MessageBaseModel();

            try
            {
                // Obtener registro de la entidad estatus que corresponde a estatus ACTIVO
                Cat_Status entStatus = new Cat_Status()
                {
                    Status_Status = "ACTIVO"
                };
                entStatus = entStatus.GetFirtByData();

                if (staffInfo.StaffId.HasValue && staffInfo.StaffId > 0)
                {
                    staffInfo.Staff_StatusId = entStatus.StatusId;
                    staffInfo.Update();
                }
                else
                {
                    Cat_Position entAlumno = new Cat_Position()
                    {
                        Position_Description = "Alumno"
                    };
                    entAlumno = entAlumno.GetFirtByData();

                    if (staffInfo.Staff_PositionId == entAlumno.PositionId)
                    {
                        IControlGenerator scg = new Utilities.StudentControlGenerator();
                        staffInfo.Staff_ControlNumber = scg.GenerateControlNumber(string.Empty);
                    }
                    else
                    {
                        Cat_Position entPos = new Cat_Position()
                        {
                            PositionId = staffInfo.Staff_PositionId
                        };
                        entPos = entPos.GetFirtByData();

                        IControlGenerator scgen = new StaffControlGenerator();
                        staffInfo.Staff_ControlNumber = scgen.GenerateControlNumber(entPos.Position_Description);
                    }

                    staffInfo.Staff_StatusId = entStatus.StatusId;
                    staffInfo.Insert();
                }
                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }

            return model;
        }

        [Route("api/StaffApi/ShowEdit")]
        [HttpGet, HttpPost]
        public StaffModel ShowEditStaff(int id)
        {
            StaffModel model = new StaffModel();

            try
            {
                Cat_Staff entObj = Cat_Staff.GetOne(id);
                model.selectedStaff = entObj;

                Cat_Status entStatus = new Cat_Status()
                {
                    Status_Status = "ACTIVO"
                };
                entStatus = entStatus.GetFirtByData();

                if (entStatus != null)
                {
                    Cat_Position entPosition = new Cat_Position()
                    {
                        Psotion_StatusId = entStatus.StatusId
                    };

                    model.lstPosition = entPosition.GetCollectionByData();
                }

                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }

            return model;
        }

        [Route("api/StaffApi/Delete")]
        [HttpGet, HttpPost]
        public MessageBaseModel DeleteStaff(int id)
        {
            MessageBaseModel model = new MessageBaseModel();

            try
            {
                Cat_Status entStatus = new Cat_Status()
                {
                    Status_Status = "INACTIVO"
                };
                entStatus = entStatus.GetFirtByData();

                Cat_Staff entObj = Cat_Staff.GetOne(id);
                entObj.Staff_StatusId = entStatus.StatusId;
                entObj.Update();

                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }

            return model;
        }
    }
}
