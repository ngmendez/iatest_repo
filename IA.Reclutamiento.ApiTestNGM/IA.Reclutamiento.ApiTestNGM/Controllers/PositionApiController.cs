﻿using System;
using System.Web.Http;
using IA.Reclutamiento.ApiTestNGM.Models;
using IA.LogicaNegocio.Entities;

namespace IA.Reclutamiento.ApiTestNGM.Controllers
{
    public class PositionApiController : ApiController
    {
        [Route("api/PositionApi/Save")]
        [HttpGet, HttpPost]
        public MessageBaseModel SavePosition(Cat_Position positionInfo)
        {
            MessageBaseModel model = new MessageBaseModel();

            try
            {
                // Obtener registro de la entidad estatus que corresponde a estatus ACTIVO
                Cat_Status entStatus = new Cat_Status()
                {
                    Status_Status = "ACTIVO"
                };
                entStatus = entStatus.GetFirtByData();

                if (positionInfo.PositionId.HasValue && positionInfo.PositionId > 0)
                {
                    positionInfo.Psotion_StatusId = entStatus.StatusId;
                    positionInfo.Update();
                }
                else
                {
                    positionInfo.Psotion_StatusId = entStatus.StatusId;
                    positionInfo.Insert();
                }
                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }

            return model;
        }

        [Route("api/PositionApi/ShowEdit")]
        [HttpGet, HttpPost]
        public PositionModel ShowEditPosition(int id)
        {
            PositionModel model = new PositionModel();

            try
            {
                Cat_Position entObj = Cat_Position.GetOne(id);
                model.selectedPosition = entObj;

                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }

            return model;
        }
        
        [Route("api/PositionApi/Delete")]
        [HttpGet, HttpPost]
        public MessageBaseModel DeleteStatus(int id)
        {
            MessageBaseModel model = new MessageBaseModel();

            try
            {
                Cat_Status entStatus = new Cat_Status()
                {
                    Status_Status = "INACTIVO"
                };
                entStatus = entStatus.GetFirtByData();

                Cat_Position entPosition = Cat_Position.GetOne(id);
                entPosition.Psotion_StatusId = entStatus.StatusId;
                entPosition.Update();

                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }

            return model;
        }
        
    }
}
