﻿using IA.LogicaNegocio.Entities;
using IA.Reclutamiento.ApiTestNGM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IA.Reclutamiento.ApiTestNGM.Controllers
{
    public class StatusApiController : ApiController
    {
        [Route("api/StatusApi/PostSaveStatus")]
        [HttpGet, HttpPost]
        public MessageBaseModel PostSaveStatus(Cat_Status statusInfo)
        {
            MessageBaseModel model = new MessageBaseModel();

            try
            {
                if (statusInfo.StatusId.HasValue && statusInfo.StatusId > 0)
                {
                    statusInfo.Update();
                }
                else
                {
                    statusInfo.Insert();
                }
                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }            

            return model;
        }

        [Route("api/StatusApi/ShowEditStatus")]
        [HttpGet, HttpPost]
        public StatusModel ShowEditStatus(int id)
        {
            StatusModel model = new StatusModel();

            try
            {
                Cat_Status entStatus = Cat_Status.GetOne(id);
                model.selectedStatus = entStatus;

                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }

            return model;
        }

        [Route("api/StatusApi/DeleteStatus")]
        [HttpGet, HttpPost]
        public MessageBaseModel DeleteStatus(int id)
        {
            MessageBaseModel model = new MessageBaseModel();

            try
            {
                Cat_Status entStatus = Cat_Status.GetOne(id);
                entStatus.Delete();

                model.Correct = true;
            }
            catch (Exception ex)
            {
                model.Correct = false;
                model.Message = ex.Message;
            }

            return model;
        }
    }
}
