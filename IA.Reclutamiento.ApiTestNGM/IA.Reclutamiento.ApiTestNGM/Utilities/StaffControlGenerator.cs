﻿using IA.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IA.Reclutamiento.ApiTestNGM.Utilities
{
    public class StaffControlGenerator : IControlGenerator
    {
        public String GenerateControlNumber(string position)
        {
            String result = String.Empty;
            Random nr = new Random();
            int sec = nr.Next(100, 999);

            switch (position)
            {
                case "Director":
                    result = "D-";
                    break;
                case "Sub-Director":
                    result = "H-";
                    break;
                case "Jefe de departamento":
                    result = "S-";
                    break;
                case "Maestro":
                    result = "T-";
                    break;
                case "Secretaria":
                    result = "A-";
                    break;
            }

            result += sec.ToString() + "-";

            for (int i = 1; i <= 3; i++)
            {
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string blocka = new string(Enumerable.Repeat(chars, 3)
                  .Select(s => s[nr.Next(s.Length)]).ToArray());

                result += blocka + "-";
            }

            result = result.TrimEnd('-');

            return result;
        }
    }
}