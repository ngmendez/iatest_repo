﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IA.Reclutamiento.ApiTestNGM.Utilities
{
    public static class General
    {
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source?.IndexOf(toCheck, comp) >= 0;
        }
    }
}