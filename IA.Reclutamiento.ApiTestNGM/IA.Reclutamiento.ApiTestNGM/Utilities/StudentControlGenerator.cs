﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IA.Reclutamiento.ApiTestNGM.Utilities
{
    public class StudentControlGenerator : IControlGenerator
    {
        public String GenerateControlNumber(string position)
        {
            String result = String.Empty;

            Random nr = new Random();
            int number = nr.Next(10000000, 99999999);

            return number.ToString();
        }
    }
}