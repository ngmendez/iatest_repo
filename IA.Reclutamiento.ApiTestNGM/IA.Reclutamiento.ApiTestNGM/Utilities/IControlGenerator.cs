﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IA.Reclutamiento.ApiTestNGM.Utilities
{
    interface IControlGenerator
    {
        String GenerateControlNumber(string position);

    }
}
