﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IA.Reclutamiento.ApiTestNGM.Models
{
    public class MessageBaseModel
    {
        public Boolean Correct { get; set; }
        public String Message { get; set; }
    }
}