﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IA.LogicaNegocio.Entities;

namespace IA.Reclutamiento.ApiTestNGM.Models
{
    public class StatusModel : MessageBaseModel
    {
        public Cat_StatusCollection lstStatus { get; set; }
        public String dsearch { get; set; }

        public Cat_Status selectedStatus = new Cat_Status();
    }
}