﻿using IA.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IA.Reclutamiento.ApiTestNGM.Models
{
    public class PositionModel : MessageBaseModel
    {
        public Cat_PositionCollection lstPosition { get; set; }
        public String dsearch { get; set; }
        public Cat_Position selectedPosition { get; set; }
    }
}