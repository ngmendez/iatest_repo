﻿using IA.LogicaNegocio.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IA.Reclutamiento.ApiTestNGM.Models
{
    public class StaffModel : MessageBaseModel
    {
        public Cat_StaffCollection lstStaff { get; set; }
        public String dsearch { get; set; }
        public Cat_Staff selectedStaff { get; set; }
        public Cat_PositionCollection lstPosition { get; set; }
    }
}