﻿$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

function filterTable(url) {
    var strStatus = $('#txt-buscar').val();

    window.location.href = url + "?dsearch=" + strStatus;

}

function btnAceptSavePosition() {

    if (fieldValid($('#txt-description')) == false) return;
    if (fieldValid($('#txt-salary-min')) == false) return;
    if (fieldValid($('#txt-salary-max')) == false) return;

    this.UrlBase = "../";
    var url = this.UrlBase + "api/PositionApi/Save";

    var jsonObj = {
        "PositionId": $('[name="position-id"]').val(),
        "Position_Description": $('[name="txt-description"]').val(),
        "Position_SalaryMin": $('[name="txt-salary-min"]').val(),
        "Position_SalaryMax": $('[name="txt-salary-max"]').val()
    };

    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(jsonObj),
        contentType: 'application/json',
        success: function (dataResponse) {
            dataContent = dataResponse;
        },
        complete: function () {
            if (dataContent.Correct) {
                setTimeout(function () {
                    window.location.href = "/Position";
                }, 500);
            } else {
                swal("Hey!", "No se pudo completar la operación, favor de intentarlo más tarde.", "error");
            }
        }
    });
}

function showEditStatus(id) {

    if (id == 0) {
        $('#modal-edit-position').modal({ backdrop: 'static', keyboard: false }).modal('show');
        return;
    }

    this.UrlBase = "../";
    var url = this.UrlBase + "api/PositionApi/ShowEdit";

    $.ajax({
        type: "GET",
        url: url,
        data: { id: id },
        beforeSend: function () {
            $('#modal-edit-position').modal({ backdrop: 'static', keyboard: false }).modal('show');
            $('#modal-edit-position .modal-title').html('Cargando...');
        },
        success: function (dataResponse) {
            $('#position-id').val(dataResponse.selectedPosition.PositionId);
            $('#txt-description').val(dataResponse.selectedPosition.Position_Description);
            $('#txt-salary-min').val(dataResponse.selectedPosition.Position_SalaryMin);
            $('#txt-salary-max').val(dataResponse.selectedPosition.Position_SalaryMax);
        },
        complete: function () {
            $('#modal-edit-position .modal-title').html('Puesto');
        }
    });
}

function deleteStatus(id) {
    this.UrlBase = "../";
    var url = this.UrlBase + "api/PositionApi/Delete";

    swal({
        title: "¿Confirma eliminar el registro?",
        text: "Ya no podrá recuperarlo después de aceptar.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function () {
        setTimeout(function () {
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    id: id
                },
                success: function (dataResponse) {
                    dataContent = dataResponse;
                },
                complete: function () {
                    if (dataContent.Correct) {
                        swal({
                            title: "Genial",
                            text: "Éxito en eliminar el registro.",
                            type: "success",
                            timer: 1000,
                            showConfirmButton: false
                        });
                        setTimeout(function () {
                            window.location.href = "/Position"
                        }, 500);
                    } else {
                        swal("Hey!", "Parece ser que no se pudo completar la operación, favor de intentarlo más tarde. Gracias.", "error");
                    }
                }
            });
        }, 1000);
    });
}