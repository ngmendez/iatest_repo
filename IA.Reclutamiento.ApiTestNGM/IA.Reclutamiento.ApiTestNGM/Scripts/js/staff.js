﻿$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

function filterTable(url) {
    var strStatus = $('#txt-buscar').val();

    window.location.href = url + "?dsearch=" + strStatus;

}

function btnAceptSaveStaff() {

    if (fieldValid($('#sel-position')) == false) return;
    if (fieldValid($('#txt-first-name')) == false) return;
    if (fieldValid($('#txt-last-name')) == false) return;
    if (fieldValid($('#txt-email')) == false) return;
    if (fieldValid($('#txt-birthdate')) == false) return;
    //if (fieldValid($('#txt-number')) == false) return;

    this.UrlBase = "../";
    var url = this.UrlBase + "api/StaffApi/Save";

    var jsonObj = {
        "StaffId": $('[name="staff-id"]').val(),
        "Staff_PositionId": $('[name="sel-position"]').val(),
        "Staff_FirstName": $('[name="txt-first-name"]').val(),
        "Staff_LastName": $('[name="txt-last-name"]').val(),
        "Staff_Email": $('[name="txt-last-name"]').val(),
        "Staff_Bithdate": $('[name="fecha-nacimiento"]').val()
        //"[Staff_ControlNumber]": $('[name="txt-number"]').val(),
    };

    console.log(jsonObj);

    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(jsonObj),
        contentType: 'application/json',
        success: function (dataResponse) {
            dataContent = dataResponse;
        },
        complete: function () {
            if (dataContent.Correct) {
                setTimeout(function () {
                    window.location.href = "/Home";
                }, 500);
            } else {
                swal("Hey!", "No se pudo completar la operación, favor de intentarlo más tarde.", "error");
            }
        }
    });
}

function showEditStaff(id) {
    this.UrlBase = "../";
    var url = this.UrlBase + "api/StaffApi/ShowEdit";

    $.ajax({
        type: "GET",
        url: url,
        data: { id: id },
        beforeSend: function () {
            $('#modal-edit-staff').modal({ backdrop: 'static', keyboard: false }).modal('show');
            $('#modal-edit-staff .modal-title').html('Cargando...');
        },
        success: function (dataResponse) {
            var dataContent = dataResponse;
            var positionId = 0;

            if (id > 0) {
                $('#staff-id').val(dataResponse.selectedStaff.StaffId);
                positionId = dataResponse.selectedStaff.Staff_PositionId;
                $('#txt-first-name').val(dataResponse.selectedStaff.Staff_FirstName);
                $('#txt-last-name').val(dataResponse.selectedStaff.Staff_LastName);
                $('#txt-email').val(dataResponse.selectedStaff.Staff_Email);
                $('#fecha-nacimiento').val(dataResponse.selectedStaff.Staff_Bithdate);
                $('#txt-number').val(dataResponse.selectedStaff.Staff_ControlNumber);
            }
            
            var select = $('#sel-position');
            $(select).html('');
            $(select).append($('<option value="" hidden>').text('--- Elige un puesto ---'));
            $.each(dataContent.lstPosition, function (index, Cat_Position) {
                console.log(Cat_Position.Position_Description);
                if (Cat_Position.PositionId == positionId) {
                    $(select).append($('<option selected>').val(Cat_Position.PositionId).text(Cat_Position.Position_Description));
                }
                else {
                    $(select).append($('<option>').val(Cat_Position.PositionId).text(Cat_Position.Position_Description));
                }
            });
        },
        complete: function () {
            $('#modal-edit-staff .modal-title').html('Personal');
        }
    });
}

function deleteStaff(id) {
    this.UrlBase = "../";
    var url = this.UrlBase + "api/StaffApi/Delete";

    swal({
        title: "¿Confirma eliminar el registro?",
        text: "Ya no podrá recuperarlo después de aceptar.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function () {
        setTimeout(function () {
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    id: id
                },
                success: function (dataResponse) {
                    dataContent = dataResponse;
                },
                complete: function () {
                    if (dataContent.Correct) {
                        swal({
                            title: "Genial",
                            text: "Éxito en eliminar el registro.",
                            type: "success",
                            timer: 1000,
                            showConfirmButton: false
                        });
                        setTimeout(function () {
                            window.location.href = "/Home"
                        }, 500);
                    } else {
                        swal("Hey!", "Parece ser que no se pudo completar la operación, favor de intentarlo más tarde. Gracias.", "error");
                    }
                }
            });
        }, 1000);
    });
}

$(document).ready(function () {
    $('#fecha-nacimiento').datepicker({
        autoclose: true,
        container: '#fecha-nacimiento-container',
        //startDate: '+1d',
        format: 'yyyy-mm-dd'
    });
});