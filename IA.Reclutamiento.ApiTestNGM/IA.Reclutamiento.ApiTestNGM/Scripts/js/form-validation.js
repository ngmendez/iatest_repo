$(document).ready(function () {
    bootstrap_alert = function () { }
    bootstrap_alert.warning = function (message) {
        $('#alert_placeholder').html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
    }
    $('*').focusout(function () {
        $(".alert").alert('close');
    });
});
/**
 * 
 */
var arrayNumbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
var arrayLetters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'i', 'z', ' '];
/*Functions for validations inputs field*/

function fieldValid(field) {
    $(".alert").alert('close');
    if (requiredValid(field) == false) return false;
    if (typeDecimalValid(field) == false) return false;	
    if (typeNumberValid(field) == false) return false;	   
    if (typeEmailValid(field) == false) return false;	    
	return true;
}

function requiredValid(field) {
	var value = $.trim($(field).val());
	$(field).val(value);
	var required = $(field).data('required'); 
    if (required && required == true && value.length == 0) {
        bootstrap_alert.warning($(field).data('message'));
		return false;
	}
	return true;
}

function typeNumberValid(field) {
    var value = $.trim($(field).val());
    var type = $(field).data('type');
    if (type && type == 'number') {
        if (isOnlyNumbers(value) == false || value == '0') {
            bootstrap_alert.warning($(field).data('message'));
            return false;
        }
    }
    return true;
}

function typeDecimalValid(field) {
    var value = $.trim($(field).val());    
    var type = $(field).data('type');
    if (type && type == 'decimal') {
        value = trimDecimalValue(value);
        $(field).val(value);
        if (isDecimalNumbers(value) == false) {
            bootstrap_alert.warning($(field).data('message'));
            return false;
        }
        if (parseFloat(value) == 0) {
            bootstrap_alert.warning($(field).data('message'));
            $(field).val('0');
            return false;
        }
    }    
    return true;
}

function trimDecimalValue(value) {
    if (value) {
        if (value.charAt(0) == '$') value = value.substr(1, value.length);
        for (var index = 0; index < value.length; index++) {
            var c = value.charAt(index);
            if (c != '0') {
                if (c != ',') {
                    return value.substr(index, value.length);
                }
            }
        }
    }    
    return value;
}

function typeEmailValid(field) {
    var value = $.trim($(field).val());
    var type = $(field).data('type');
    if (type && type == 'email') {
        if (value.length > 0 && isEmailValid(value) == false) {
            bootstrap_alert.warning($(field).data('message'));
            return false;
        }
    }

    return true;
}

function isOnlyNumbers(value) {
	for(var index= 0; index < value.length; index++) {
		if(!arrayNumbers.includes(value.charAt(index))) {
			return false;
		}
	}
	return true;
}

function isOnlyLetters(value) {
	for(var index= 0; index < value.length; index++) {
		if(!arrayLetters.includes(value.charAt(index))) {
			return false;
		}
	}
	return true;
}

function isOnlyNumbersOrLetters(value) {
	for(var index= 0; index < value.length; index++) {
		if(!arrayNumbers.includes(value.charAt(index)) && !arrayLetters.includes(value.charAt(index))) {
			return false;
		}
	}
	return true;
}

function isDecimalNumbers(value) {
    var re = /(\d{1,})|((\d+(\,|\.))+\d{2,})$/;
    return re.test(value);
}

function isEmailValid(value) {
    var re = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
	return re.test(value);
}
/*--------------------------------------------*/

/*Functions for events input onkeypress, onkeyup,...*/

function isNumber(event) {
	var key =  event.which || event.keyCode;
	return (key >= 48 && key <= 57);
}

function isLetter(event) {
	var key =  event.which || event.keyCode;
	return ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || isSpacerOrEnter(key));
}

function isAlphaNumeric(event) {
	var key =  event.which || event.keyCode;
	return (isNumber(event) || isLetter(event));
}

function isSpacerOrEnter(key) {
	 return (key==32 || key==13);
}

function isValidLength(text, maxLenght) {
	return text.length <= maxLenght;
}

/*--------------------------------------------*/

/*Functions pluging form validations and reset*/
$.fn.formvalidate = function () {
    var fieldsValids = true;
	var inputComponents = $(this).find('[data-toggle="validation"]');	
	$.each(inputComponents, function(index, field) {		
        if (fieldValid(field) == false) {
            fieldsValids = false;
            return false;
		}
	});
    return fieldsValids;
}