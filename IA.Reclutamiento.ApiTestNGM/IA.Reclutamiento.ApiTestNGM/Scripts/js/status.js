﻿$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

function filterTable(url) {
    var strStatus = $('#txt-buscar').val();

    window.location.href = url + "?dsearch=" + strStatus;

}

function btnAceptSaveStatus() {

    if (fieldValid($('#txt-status')) == false) return;

    this.UrlBase = "../";
    var url = this.UrlBase + "api/StatusApi/PostSaveStatus";

    var jsonObj = {
        "StatusId": $('[name="status-id"]').val(),
        "Status_Status": $('[name="txt-status"]').val()
    };
    console.log(jsonObj);
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(jsonObj),
        contentType: 'application/json',
        success: function (dataResponse) {
            dataContent = dataResponse;
        },
        complete: function () {
            if (dataContent.Correct) {
                setTimeout(function () {
                    window.location.href = "/Status";
                }, 500);
            } else {
                swal("Hey!", "No se pudo completar la operación, favor de intentarlo más tarde.", "error");
            }
        }
    });
}

function showEditStatus(id) {

    if (id == 0) {
        $('#modal-edit-status').modal({ backdrop: 'static', keyboard: false }).modal('show');
        return;
    }

    this.UrlBase = "../";
    var url = this.UrlBase + "api/StatusApi/ShowEditStatus";

    $.ajax({
        type: "GET",
        url: url,
        data: { id: id },
        beforeSend: function () {
            $('#modal-edit-status').modal({ backdrop: 'static', keyboard: false }).modal('show');
            $('#modal-edit-status .modal-title').html('Cargando...');
        },
        success: function (dataResponse) {
            $('#status-id').val(dataResponse.selectedStatus.StatusId);
            $('#txt-status').val(dataResponse.selectedStatus.Status_Status);
        },
        complete: function () {
            $('#modal-edit-status .modal-title').html('Estatus');
        }
    });
}

function deleteStatus(id) {
    this.UrlBase = "../";
    var url = this.UrlBase + "api/StatusApi/DeleteStatus";

    swal({
        title: "¿Confirma eliminar el registro?",
        text: "Ya no podrá recuperarlo después de aceptar.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function () {
        setTimeout(function () {
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    id: id
                },
                success: function (dataResponse) {
                    dataContent = dataResponse;
                },
                complete: function () {
                    if (dataContent.Correct) {
                        swal({
                            title: "Genial",
                            text: "Éxito en eliminar el registro.",
                            type: "success",
                            timer: 1000,
                            showConfirmButton: false
                        });
                        setTimeout(function () {
                            window.location.href = "/Status"
                        }, 500);
                    } else {
                        swal("Hey!", "Parece ser que no se pudo completar la operación, favor de intentarlo más tarde. Gracias.", "error");
                    }
                }
            });
        }, 1000);
    });
}