USE [master]
GO
/****** Object:  Database [DB_IATest_dev]    Script Date: 01/05/2019 07:10:58 p. m. ******/
CREATE DATABASE [DB_IATest_dev]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DB_IATest_dev', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DB_IATest_dev.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DB_IATest_dev_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DB_IATest_dev_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [DB_IATest_dev] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DB_IATest_dev].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DB_IATest_dev] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET ARITHABORT OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DB_IATest_dev] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DB_IATest_dev] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DB_IATest_dev] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DB_IATest_dev] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET RECOVERY FULL 
GO
ALTER DATABASE [DB_IATest_dev] SET  MULTI_USER 
GO
ALTER DATABASE [DB_IATest_dev] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DB_IATest_dev] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DB_IATest_dev] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DB_IATest_dev] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DB_IATest_dev] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DB_IATest_dev', N'ON'
GO
ALTER DATABASE [DB_IATest_dev] SET QUERY_STORE = OFF
GO
USE [DB_IATest_dev]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 01/05/2019 07:10:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_cat_Position]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_cat_Position](
	[PositionId] [int] IDENTITY(1,1) NOT NULL,
	[Position_Description] [varchar](250) NULL,
	[Position_SalaryMin] [float] NULL,
	[Position_SalaryMax] [float] NULL,
	[Psotion_StatusId] [int] NULL,
 CONSTRAINT [PK_tbl_cat_Position] PRIMARY KEY CLUSTERED 
(
	[PositionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_cat_Staff]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_cat_Staff](
	[StaffId] [int] IDENTITY(1,1) NOT NULL,
	[Staff_PositionId] [int] NULL,
	[Staff_StatusId] [int] NULL,
	[Staff_FirstName] [varchar](250) NULL,
	[Staff_LastName] [varchar](250) NULL,
	[Staff_Email] [varchar](250) NULL,
	[Staff_Bithdate] [date] NULL,
	[Staff_ControlNumber] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_cat_Staff] PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_cat_Status]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_cat_Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status_Status] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_cat_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
/****** Object:  StoredProcedure [dbo].[prc_AspNetUsers_GetAll]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_AspNetUsers_GetAll]
AS 
BEGIN
SET NOCOUNT ON

SELECT
    [Id],
    [Email],
    [EmailConfirmed],
    [PasswordHash],
    [SecurityStamp],
    [PhoneNumber],
    [PhoneNumberConfirmed],
    [TwoFactorEnabled],
    [LockoutEndDateUtc],
    [LockoutEnabled],
    [AccessFailedCount],
    [UserName]
FROM [dbo].[AspNetUsers] WITH (NOLOCK)

END
GO
/****** Object:  StoredProcedure [dbo].[prc_AspNetUsers_GetByData]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_AspNetUsers_GetByData]
    @Id nvarchar(128) = Null,
    @Email nvarchar(256) = Null,
    @EmailConfirmed bit = Null,
    @PasswordHash nvarchar(MAX) = Null,
    @SecurityStamp nvarchar(MAX) = Null,
    @PhoneNumber nvarchar(MAX) = Null,
    @PhoneNumberConfirmed bit = Null,
    @TwoFactorEnabled bit = Null,
    @LockoutEndDateUtc datetime = Null,
    @LockoutEnabled bit = Null,
    @AccessFailedCount int = Null,
    @UserName nvarchar(256) = Null
AS
BEGIN
SET NOCOUNT ON

DECLARE @Query nvarchar(MAX)
SET @Query = 
'SELECT
    [Id],
    [Email],
    [EmailConfirmed],
    [PasswordHash],
    [SecurityStamp],
    [PhoneNumber],
    [PhoneNumberConfirmed],
    [TwoFactorEnabled],
    [LockoutEndDateUtc],
    [LockoutEnabled],
    [AccessFailedCount],
    [UserName]
FROM [dbo].[AspNetUsers] WITH (NOLOCK)'

DECLARE @QueryWhere nvarchar(MAX)
SET @QueryWhere = ''
DECLARE @NlAnd nvarchar(10)
SET @NlAnd = '
AND '

if(@Id IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Id] = @Id'if(@Email IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Email] = @Email'if(@EmailConfirmed IS NOT NULL)
    set @QueryWhere += @NlAnd + '[EmailConfirmed] = @EmailConfirmed'if(@PasswordHash IS NOT NULL)
    set @QueryWhere += @NlAnd + '[PasswordHash] = @PasswordHash'if(@SecurityStamp IS NOT NULL)
    set @QueryWhere += @NlAnd + '[SecurityStamp] = @SecurityStamp'if(@PhoneNumber IS NOT NULL)
    set @QueryWhere += @NlAnd + '[PhoneNumber] = @PhoneNumber'if(@PhoneNumberConfirmed IS NOT NULL)
    set @QueryWhere += @NlAnd + '[PhoneNumberConfirmed] = @PhoneNumberConfirmed'if(@TwoFactorEnabled IS NOT NULL)
    set @QueryWhere += @NlAnd + '[TwoFactorEnabled] = @TwoFactorEnabled'if(@LockoutEndDateUtc IS NOT NULL)
    set @QueryWhere += @NlAnd + '[LockoutEndDateUtc] = @LockoutEndDateUtc'if(@LockoutEnabled IS NOT NULL)
    set @QueryWhere += @NlAnd + '[LockoutEnabled] = @LockoutEnabled'if(@AccessFailedCount IS NOT NULL)
    set @QueryWhere += @NlAnd + '[AccessFailedCount] = @AccessFailedCount'if(@UserName IS NOT NULL)
    set @QueryWhere += @NlAnd + '[UserName] = @UserName'
if(@QueryWhere != '')
    set @QueryWhere = STUFF(@QueryWhere, 2 , 4, 'WHERE')

set @Query += @QueryWhere
--print @Query
exec sp_executesql
@Query,
N'    @Id nvarchar(128) = Null,
    @Email nvarchar(256) = Null,
    @EmailConfirmed bit = Null,
    @PasswordHash nvarchar(MAX) = Null,
    @SecurityStamp nvarchar(MAX) = Null,
    @PhoneNumber nvarchar(MAX) = Null,
    @PhoneNumberConfirmed bit = Null,
    @TwoFactorEnabled bit = Null,
    @LockoutEndDateUtc datetime = Null,
    @LockoutEnabled bit = Null,
    @AccessFailedCount int = Null,
    @UserName nvarchar(256) = Null',
@Id = @Id,
@Email = @Email,
@EmailConfirmed = @EmailConfirmed,
@PasswordHash = @PasswordHash,
@SecurityStamp = @SecurityStamp,
@PhoneNumber = @PhoneNumber,
@PhoneNumberConfirmed = @PhoneNumberConfirmed,
@TwoFactorEnabled = @TwoFactorEnabled,
@LockoutEndDateUtc = @LockoutEndDateUtc,
@LockoutEnabled = @LockoutEnabled,
@AccessFailedCount = @AccessFailedCount,
@UserName = @UserName

END
GO
/****** Object:  StoredProcedure [dbo].[prc_AspNetUsers_GetOne]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_AspNetUsers_GetOne]
    @Id nvarchar(128)
AS
BEGIN
SET NOCOUNT ON

SELECT
    [Id],
    [Email],
    [EmailConfirmed],
    [PasswordHash],
    [SecurityStamp],
    [PhoneNumber],
    [PhoneNumberConfirmed],
    [TwoFactorEnabled],
    [LockoutEndDateUtc],
    [LockoutEnabled],
    [AccessFailedCount],
    [UserName]
FROM [dbo].[AspNetUsers] WITH (NOLOCK)
WHERE [Id] = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Position_Delete]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Position_Delete]
    @PositionId int
AS
BEGIN
SET NOCOUNT ON

DELETE FROM [dbo].[tbl_cat_Position]
WHERE [PositionId] = @PositionId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Position_GetAll]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Position_GetAll]
AS 
BEGIN
SET NOCOUNT ON

SELECT
    [PositionId],
    [Position_Description],
    [Position_SalaryMin],
    [Position_SalaryMax],
    [Psotion_StatusId]
FROM [dbo].[tbl_cat_Position] WITH (NOLOCK)

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Position_GetByData]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Position_GetByData]
    @PositionId int = Null,
    @Position_Description varchar(250) = Null,
    @Position_SalaryMin float(53) = Null,
    @Position_SalaryMax float(53) = Null,
    @Psotion_StatusId int = Null
AS
BEGIN
SET NOCOUNT ON

DECLARE @Query nvarchar(MAX)
SET @Query = 
'SELECT
    [PositionId],
    [Position_Description],
    [Position_SalaryMin],
    [Position_SalaryMax],
    [Psotion_StatusId]
FROM [dbo].[tbl_cat_Position] WITH (NOLOCK)'

DECLARE @QueryWhere nvarchar(MAX)
SET @QueryWhere = ''
DECLARE @NlAnd nvarchar(10)
SET @NlAnd = '
AND '

if(@PositionId IS NOT NULL)
    set @QueryWhere += @NlAnd + '[PositionId] = @PositionId'if(@Position_Description IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Position_Description] = @Position_Description'if(@Position_SalaryMin IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Position_SalaryMin] = @Position_SalaryMin'if(@Position_SalaryMax IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Position_SalaryMax] = @Position_SalaryMax'if(@Psotion_StatusId IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Psotion_StatusId] = @Psotion_StatusId'
if(@QueryWhere != '')
    set @QueryWhere = STUFF(@QueryWhere, 2 , 4, 'WHERE')

set @Query += @QueryWhere
--print @Query
exec sp_executesql
@Query,
N'    @PositionId int = Null,
    @Position_Description varchar(250) = Null,
    @Position_SalaryMin float(53) = Null,
    @Position_SalaryMax float(53) = Null,
    @Psotion_StatusId int = Null',
@PositionId = @PositionId,
@Position_Description = @Position_Description,
@Position_SalaryMin = @Position_SalaryMin,
@Position_SalaryMax = @Position_SalaryMax,
@Psotion_StatusId = @Psotion_StatusId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Position_GetOne]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Position_GetOne]
    @PositionId int
AS
BEGIN
SET NOCOUNT ON

SELECT
    [PositionId],
    [Position_Description],
    [Position_SalaryMin],
    [Position_SalaryMax],
    [Psotion_StatusId]
FROM [dbo].[tbl_cat_Position] WITH (NOLOCK)
WHERE [PositionId] = @PositionId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Position_Insert]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Position_Insert]
    @Position_Description varchar(250) = NULL,
    @Position_SalaryMin float(53) = NULL,
    @Position_SalaryMax float(53) = NULL,
    @Psotion_StatusId int = NULL
AS
BEGIN
SET NOCOUNT ON

INSERT INTO [dbo].[tbl_cat_Position] WITH (ROWLOCK)(
    [Position_Description],
    [Position_SalaryMin],
    [Position_SalaryMax],
    [Psotion_StatusId]
)
values(
    @Position_Description,
    @Position_SalaryMin,
    @Position_SalaryMax,
    @Psotion_StatusId
);

SELECT CAST(SCOPE_IDENTITY() as int);

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Position_Update]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Position_Update]
    @PositionId int,
    @Position_Description varchar(250) = NULL,
    @Position_SalaryMin float(53) = NULL,
    @Position_SalaryMax float(53) = NULL,
    @Psotion_StatusId int = NULL
AS
BEGIN
SET NOCOUNT ON

UPDATE [dbo].[tbl_cat_Position] WITH (ROWLOCK) SET
    [Position_Description] = @Position_Description,
    [Position_SalaryMin] = @Position_SalaryMin,
    [Position_SalaryMax] = @Position_SalaryMax,
    [Psotion_StatusId] = @Psotion_StatusId
WHERE [PositionId] = @PositionId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Staff_Delete]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Staff_Delete]
    @StaffId int
AS
BEGIN
SET NOCOUNT ON

DELETE FROM [dbo].[tbl_cat_Staff]
WHERE [StaffId] = @StaffId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Staff_GetAll]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Staff_GetAll]
AS 
BEGIN
SET NOCOUNT ON

SELECT
    [StaffId],
    [Staff_PositionId],
    [Staff_StatusId],
    [Staff_FirstName],
    [Staff_LastName],
    [Staff_Email],
    [Staff_Bithdate],
    [Staff_ControlNumber]
FROM [dbo].[tbl_cat_Staff] WITH (NOLOCK)

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Staff_GetByData]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Staff_GetByData]
    @StaffId int = Null,
    @Staff_PositionId int = Null,
    @Staff_StatusId int = Null,
    @Staff_FirstName varchar(250) = Null,
    @Staff_LastName varchar(250) = Null,
    @Staff_Email varchar(250) = Null,
    @Staff_Bithdate date = Null,
    @Staff_ControlNumber varchar(50) = Null
AS
BEGIN
SET NOCOUNT ON

DECLARE @Query nvarchar(MAX)
SET @Query = 
'SELECT
    [StaffId],
    [Staff_PositionId],
    [Staff_StatusId],
    [Staff_FirstName],
    [Staff_LastName],
    [Staff_Email],
    [Staff_Bithdate],
    [Staff_ControlNumber]
FROM [dbo].[tbl_cat_Staff] WITH (NOLOCK)'

DECLARE @QueryWhere nvarchar(MAX)
SET @QueryWhere = ''
DECLARE @NlAnd nvarchar(10)
SET @NlAnd = '
AND '

if(@StaffId IS NOT NULL)
    set @QueryWhere += @NlAnd + '[StaffId] = @StaffId'if(@Staff_PositionId IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Staff_PositionId] = @Staff_PositionId'if(@Staff_StatusId IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Staff_StatusId] = @Staff_StatusId'if(@Staff_FirstName IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Staff_FirstName] = @Staff_FirstName'if(@Staff_LastName IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Staff_LastName] = @Staff_LastName'if(@Staff_Email IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Staff_Email] = @Staff_Email'if(@Staff_Bithdate IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Staff_Bithdate] = @Staff_Bithdate'if(@Staff_ControlNumber IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Staff_ControlNumber] = @Staff_ControlNumber'
if(@QueryWhere != '')
    set @QueryWhere = STUFF(@QueryWhere, 2 , 4, 'WHERE')

set @Query += @QueryWhere
--print @Query
exec sp_executesql
@Query,
N'    @StaffId int = Null,
    @Staff_PositionId int = Null,
    @Staff_StatusId int = Null,
    @Staff_FirstName varchar(250) = Null,
    @Staff_LastName varchar(250) = Null,
    @Staff_Email varchar(250) = Null,
    @Staff_Bithdate date = Null,
    @Staff_ControlNumber varchar(50) = Null',
@StaffId = @StaffId,
@Staff_PositionId = @Staff_PositionId,
@Staff_StatusId = @Staff_StatusId,
@Staff_FirstName = @Staff_FirstName,
@Staff_LastName = @Staff_LastName,
@Staff_Email = @Staff_Email,
@Staff_Bithdate = @Staff_Bithdate,
@Staff_ControlNumber = @Staff_ControlNumber

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Staff_GetOne]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Staff_GetOne]
    @StaffId int
AS
BEGIN
SET NOCOUNT ON

SELECT
    [StaffId],
    [Staff_PositionId],
    [Staff_StatusId],
    [Staff_FirstName],
    [Staff_LastName],
    [Staff_Email],
    [Staff_Bithdate],
    [Staff_ControlNumber]
FROM [dbo].[tbl_cat_Staff] WITH (NOLOCK)
WHERE [StaffId] = @StaffId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Staff_Insert]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Staff_Insert]
    @Staff_PositionId int = NULL,
    @Staff_StatusId int = NULL,
    @Staff_FirstName varchar(250) = NULL,
    @Staff_LastName varchar(250) = NULL,
    @Staff_Email varchar(250) = NULL,
    @Staff_Bithdate date = NULL,
    @Staff_ControlNumber varchar(50) = NULL
AS
BEGIN
SET NOCOUNT ON

INSERT INTO [dbo].[tbl_cat_Staff] WITH (ROWLOCK)(
    [Staff_PositionId],
    [Staff_StatusId],
    [Staff_FirstName],
    [Staff_LastName],
    [Staff_Email],
    [Staff_Bithdate],
    [Staff_ControlNumber]
)
values(
    @Staff_PositionId,
    @Staff_StatusId,
    @Staff_FirstName,
    @Staff_LastName,
    @Staff_Email,
    @Staff_Bithdate,
    @Staff_ControlNumber
);

SELECT CAST(SCOPE_IDENTITY() as int);

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Staff_Update]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Staff_Update]
    @StaffId int,
    @Staff_PositionId int = NULL,
    @Staff_StatusId int = NULL,
    @Staff_FirstName varchar(250) = NULL,
    @Staff_LastName varchar(250) = NULL,
    @Staff_Email varchar(250) = NULL,
    @Staff_Bithdate date = NULL,
    @Staff_ControlNumber varchar(50) = NULL
AS
BEGIN
SET NOCOUNT ON

UPDATE [dbo].[tbl_cat_Staff] WITH (ROWLOCK) SET
    [Staff_PositionId] = @Staff_PositionId,
    [Staff_StatusId] = @Staff_StatusId,
    [Staff_FirstName] = @Staff_FirstName,
    [Staff_LastName] = @Staff_LastName,
    [Staff_Email] = @Staff_Email,
    [Staff_Bithdate] = @Staff_Bithdate,
    [Staff_ControlNumber] = @Staff_ControlNumber
WHERE [StaffId] = @StaffId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Status_Delete]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Status_Delete]
    @StatusId int
AS
BEGIN
SET NOCOUNT ON

DELETE FROM [dbo].[tbl_cat_Status]
WHERE [StatusId] = @StatusId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Status_GetAll]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Status_GetAll]
AS 
BEGIN
SET NOCOUNT ON

SELECT
    [StatusId],
    [Status_Status]
FROM [dbo].[tbl_cat_Status] WITH (NOLOCK)

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Status_GetByData]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Status_GetByData]
    @StatusId int = Null,
    @Status_Status varchar(50) = Null
AS
BEGIN
SET NOCOUNT ON

DECLARE @Query nvarchar(MAX)
SET @Query = 
'SELECT
    [StatusId],
    [Status_Status]
FROM [dbo].[tbl_cat_Status] WITH (NOLOCK)'

DECLARE @QueryWhere nvarchar(MAX)
SET @QueryWhere = ''
DECLARE @NlAnd nvarchar(10)
SET @NlAnd = '
AND '

if(@StatusId IS NOT NULL)
    set @QueryWhere += @NlAnd + '[StatusId] = @StatusId'if(@Status_Status IS NOT NULL)
    set @QueryWhere += @NlAnd + '[Status_Status] = @Status_Status'
if(@QueryWhere != '')
    set @QueryWhere = STUFF(@QueryWhere, 2 , 4, 'WHERE')

set @Query += @QueryWhere
--print @Query
exec sp_executesql
@Query,
N'    @StatusId int = Null,
    @Status_Status varchar(50) = Null',
@StatusId = @StatusId,
@Status_Status = @Status_Status

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Status_GetOne]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Status_GetOne]
    @StatusId int
AS
BEGIN
SET NOCOUNT ON

SELECT
    [StatusId],
    [Status_Status]
FROM [dbo].[tbl_cat_Status] WITH (NOLOCK)
WHERE [StatusId] = @StatusId

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Status_Insert]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Status_Insert]
    @Status_Status varchar(50) = NULL
AS
BEGIN
SET NOCOUNT ON

INSERT INTO [dbo].[tbl_cat_Status] WITH (ROWLOCK)(
    [Status_Status]
)
values(
    @Status_Status
);

SELECT CAST(SCOPE_IDENTITY() as int);

END
GO
/****** Object:  StoredProcedure [dbo].[prc_tbl_cat_Status_Update]    Script Date: 01/05/2019 07:10:59 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prc_tbl_cat_Status_Update]
    @StatusId int,
    @Status_Status varchar(50) = NULL
AS
BEGIN
SET NOCOUNT ON

UPDATE [dbo].[tbl_cat_Status] WITH (ROWLOCK) SET
    [Status_Status] = @Status_Status
WHERE [StatusId] = @StatusId

END
GO
USE [master]
GO
ALTER DATABASE [DB_IATest_dev] SET  READ_WRITE 
GO
